// =========================================================================
// name: randperm.c
// desc: extern for pd
//          'randperm n' 
//	    	 takes in a bang on the left inlet 
//                  and outputs a random permutation from 0 to n-1 as a list
// 		 enter a new float on the right inlet to change n
//               send a message with a list of numbers to the left inlet
//                  to randomly permute the list
//		      
//
// author: jennifer hsu
// date: fall 2013 
// ==========================================================================

#include "m_pd.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>

#define MAX_LIST_LENGTH 500

// struct type to hold all variables that have to do with the randperm object
typedef struct randperm
{
  t_object rp_obj;
  t_outlet *rp_outlet;
  int maxNum;
  t_atom shuffle_list[MAX_LIST_LENGTH];
} t_randperm;

// randperm class that will be created in 'setup'
// and used in 'new to create new instances
t_class *randperm_class;

// function prototypes 
void randperm_bang(t_randperm *randperm_ptr);
void randperm_rightInlet(t_randperm *randperm_ptr, t_floatarg f);
void randperm_list(t_randperm *randperm_ptr, t_symbol *selector, int argc, t_atom *argv);
void *randperm_new(t_floatarg initialMaxNum);
void randperm_setup(void);

// bang callback
void randperm_bang(t_randperm *randperm_ptr)
{
  int argc = randperm_ptr->maxNum;

  int i;
  // shuffle the list and swap based on random numbers
  for(i = 0; i < argc; i++)
  {
    int newInd = rand() % argc;
    t_float tempNumAtnewInd = atom_getfloatarg(newInd, argc, randperm_ptr->shuffle_list);
    t_float tempNumAtoldInd = atom_getfloatarg(i, argc, randperm_ptr->shuffle_list);
    // swap
    SETFLOAT(randperm_ptr->shuffle_list+i, tempNumAtnewInd);
    SETFLOAT(randperm_ptr->shuffle_list+newInd, tempNumAtoldInd);
  }

  outlet_list(randperm_ptr->rp_outlet, gensym("list"), argc, randperm_ptr->shuffle_list);
}

// right inlet with float callback
void randperm_rightInlet(t_randperm *randperm_ptr, t_floatarg f)
{
  if(f < 0)
  {
    post("please enter a positive number");
    return;
  }

  randperm_ptr->maxNum = ceil(f);
  int i;
  // create a list from 0 to maxNum-1 in our shuffle_list
  for(i = 0; i < randperm_ptr->maxNum; i++)
  {
    SETFLOAT(randperm_ptr->shuffle_list+i, i);
  }

}

// list callback - shuffle the user-provided list
void randperm_list(t_randperm *randperm_ptr, t_symbol *selector, int argc, t_atom *argv)
{
  // update internal pointer about list length
  randperm_ptr->maxNum = argc;

  int i;
  // copy list into our outlet list
  for(i = 0; i < argc; i++)
  {
    // error check
    if(argv[i].a_type != A_FLOAT)
    {
      post("arguments in list must be numbers");
      return;
    }
    SETFLOAT(randperm_ptr->shuffle_list+i, atom_getfloatarg(i, argc, argv));
  }

  // use random permutation function from our bang function
  randperm_bang(randperm_ptr);
}

// new (constructor) function
void *randperm_new(t_floatarg initialMaxNum)
{
  // make a new randperm object from class created in setup function
  t_randperm *rp_new = (t_randperm *)pd_new(randperm_class);
  // create a new inlet
  inlet_new(&rp_new->rp_obj, &rp_new->rp_obj.ob_pd, gensym("float"), gensym("ftRight"));
  // create a new outlet, save pointer returned
  rp_new->rp_outlet = outlet_new(&rp_new->rp_obj, gensym("list"));

  // error check
  if(initialMaxNum < 0) 
  {
    post("number entered must be positive.");
    initialMaxNum = 1;
  }
  // set maxNum to initial number
  rp_new->maxNum = ceil(initialMaxNum);

  // initialize shuffle list based on creation argument
  int i;
  for(i = 0; i < rp_new->maxNum; i++)
  {
    SETFLOAT(rp_new->shuffle_list+i, i);
  }

  // initialize random seed
  srand(time(NULL));
  
  return (void *)rp_new;
}

// setup function
void randperm_setup(void)
{
  // create a new class
  randperm_class = class_new(gensym("randperm"), (t_newmethod)randperm_new, 0, sizeof(t_randperm), 0, A_DEFFLOAT, 0);
  // register the bang callback
  class_addbang(randperm_class, (t_method)randperm_bang);
  // register callback method for right inlet (float)
  class_addmethod(randperm_class, (t_method)randperm_rightInlet, gensym("ftRight"), A_FLOAT, 0);
  // register the list callback
  class_addlist(randperm_class, (t_method)randperm_list);
}
