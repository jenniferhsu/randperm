Jennifer Hsu (jsh008@ucsd.edu)
Music 267 - Assignment 1
Date: October 29, 2013

Pd external that randomly permutes a list of numbers. Given a single number, the randperm will permute numbers 0 to number-1.  Given a list of numbers, randperm will permute that entire list.  Currently, randperm can only permute up to 500 numbers.  

Developed for Pd-0.45-2.

Compile the files:

1. Please modify your makefile inside the \{directory_with_pd\}/Contents/Resources/doc/6.externs to look like mine (just adding my c file - randperm.c to the list)
2. Copy or  move randperm.c and interpFiltFFT~.c into the \{directory_with_pd\}/Contents/Resources/doc/6.externs directory
3. Open up a terminal and type "make pd_darwin"
4. Copy or move randperm.pd_darwin and interpFiltFFT~.pd_darwin into \{directory_with_pd\}/Contents/Resources/extra directory


For using the external, please see the included example patch.
